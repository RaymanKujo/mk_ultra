function suite(destination){
    /* équivalent en jQuery de document.querySelector("#sel1") */
    /* sauf que le résultat est un objet de type jQuery        */
    var t = $("#titre");
    var c = $("#contenu");
    var h = $("#choix");
    $.get("suivant", {data: destination})
        .done(function(data){
            // console.log("succès", data, "div =", div);
            t.html(data.contexte.Titre);
            c.html(data.contexte.scene);
            var leschoix = data.contexte.suivants;
            // on enlève les anciens boutons
            h.find("button").remove()
            for (var i=0; i<leschoix.length; i++){
                /* contrepartie en jQuery de document.createElement */
                var b = $("<button>");
                b.attr("onclick", 'suite("' + leschoix[i] + '")');
                b.html("Je vais en  " + leschoix[i]);
                h.append(b);
            }
        })
        .fail(function(data){
            console.log("échec", data);
            
        });
}
