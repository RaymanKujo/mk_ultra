import cherrypy
from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))


jdr = {
    "A": {"Titre": "Titre du A", "scene": "Scène du A", "suivants" : ["C","D"]},
    "B": {"Titre": "Titre du B", "scene": "Scène du B", "suivants" : ["D","A"]},
    "C": {"Titre": "Titre du C", "scene": "Scène du C", "suivants" : ["B","D"]},
    "D": {"Titre": "Titre du D", "scene": "Scène du D", "suivants" : ["C","B"]},
    
}
class HelloWorld(object):
    @cherrypy.expose
    def index(self):
        modele = env.get_template('index.html')
        return modele.render()
       
    @cherrypy.tools.json_out()
    @cherrypy.expose
    def suivant(self, data):
    
        return {"contexte" : jdr[data]}

if __name__ == '__main__':
    import os
    static_dir= os.path.abspath(
        os.path.join(os.path.dirname(__file__), "static")
    )
    conf = {
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir':  static_dir,
        },
    }
    
    cherrypy.config.update(conf)
    cherrypy.config.update({
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8080,
    })
    cherrypy.quickstart(HelloWorld(), '/', config = conf)
